<?php
/**
 * @file
 * appstore_feature.views_default.inc
 */

/**
 * Implementation of hook_views_default_views().
 */
function appstore_feature_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'apps';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'apps';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 1;
  /* Relationship: Content: Referenced product */
  $handler->display->display_options['relationships']['field_app_release_reference_product_id']['id'] = 'field_app_release_reference_product_id';
  $handler->display->display_options['relationships']['field_app_release_reference_product_id']['table'] = 'field_data_field_app_release_reference';
  $handler->display->display_options['relationships']['field_app_release_reference_product_id']['field'] = 'field_app_release_reference_product_id';
  $handler->display->display_options['relationships']['field_app_release_reference_product_id']['required'] = 0;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_image']['id'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['table'] = 'field_data_field_image';
  $handler->display->display_options['fields']['field_image']['field'] = 'field_image';
  $handler->display->display_options['fields']['field_image']['label'] = '';
  $handler->display->display_options['fields']['field_image']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_image']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_image']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_image']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_image']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_image']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_image']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_image']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => 'content',
  );
  $handler->display->display_options['fields']['field_image']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Field: Content: Fivestars */
  $handler->display->display_options['fields']['field_fivestars_2']['id'] = 'field_fivestars_2';
  $handler->display->display_options['fields']['field_fivestars_2']['table'] = 'field_data_field_fivestars';
  $handler->display->display_options['fields']['field_fivestars_2']['field'] = 'field_fivestars';
  $handler->display->display_options['fields']['field_fivestars_2']['label'] = '';
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_fivestars_2']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_fivestars_2']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_fivestars_2']['click_sort_column'] = 'rating';
  $handler->display->display_options['fields']['field_fivestars_2']['settings'] = array(
    'widget' => array(
      'fivestar_widget' => 'sites/all/modules/contrib/fivestar/widgets/basic/basic.css',
    ),
    'expose' => 1,
    'style' => 'average',
    'text' => 'none',
  );
  $handler->display->display_options['fields']['field_fivestars_2']['field_api_classes'] = 0;
  /* Field: Commerce Product: Price */
  $handler->display->display_options['fields']['commerce_price']['id'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['table'] = 'field_data_commerce_price';
  $handler->display->display_options['fields']['commerce_price']['field'] = 'commerce_price';
  $handler->display->display_options['fields']['commerce_price']['relationship'] = 'field_app_release_reference_product_id';
  $handler->display->display_options['fields']['commerce_price']['label'] = '';
  $handler->display->display_options['fields']['commerce_price']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['external'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['commerce_price']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['commerce_price']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['commerce_price']['alter']['html'] = 0;
  $handler->display->display_options['fields']['commerce_price']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['commerce_price']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['commerce_price']['hide_empty'] = 0;
  $handler->display->display_options['fields']['commerce_price']['empty_zero'] = 0;
  $handler->display->display_options['fields']['commerce_price']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['commerce_price']['click_sort_column'] = 'amount';
  $handler->display->display_options['fields']['commerce_price']['type'] = 'commerce_price_formatted_amount';
  $handler->display->display_options['fields']['commerce_price']['settings'] = array(
    'calculation' => '0',
  );
  $handler->display->display_options['fields']['commerce_price']['field_api_classes'] = 0;
  /* Field: Content: App Release Reference */
  $handler->display->display_options['fields']['field_app_release_reference']['id'] = 'field_app_release_reference';
  $handler->display->display_options['fields']['field_app_release_reference']['table'] = 'field_data_field_app_release_reference';
  $handler->display->display_options['fields']['field_app_release_reference']['field'] = 'field_app_release_reference';
  $handler->display->display_options['fields']['field_app_release_reference']['label'] = '';
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_app_release_reference']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_app_release_reference']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_app_release_reference']['settings'] = array(
    'show_quantity' => 0,
    'default_quantity' => '1',
    'combine' => 1,
    'line_item_type' => 0,
  );
  $handler->display->display_options['fields']['field_app_release_reference']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'application' => 'application',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'apps';
  $export['apps'] = $view;

  return $export;
}
