<?php
/**
 * @file
 * appstore_feature.features.inc
 */

/**
 * Implementation of hook_commerce_product_default_types().
 */
function appstore_feature_commerce_product_default_types() {
  $items = array(
    'app_release' => array(
      'type' => 'app_release',
      'name' => 'App Release',
      'description' => 'This app product has a commerce file field attached to sell files.',
      'help' => '',
      'module' => 'commerce_product_ui',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_ctools_plugin_api().
 */
function appstore_feature_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}

/**
 * Implementation of hook_views_api().
 */
function appstore_feature_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implementation of hook_node_info().
 */
function appstore_feature_node_info() {
  $items = array(
    'application' => array(
      'name' => t('Application'),
      'base' => 'node_content',
      'description' => t('This content type allows for displaying your application products through a product reference field. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
