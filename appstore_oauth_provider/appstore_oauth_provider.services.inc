<?php

/**
 * @file
 * Export of OAuth provider's Services endpoint
 */

/**
 * Implementation of hook_default_services_endpoint().
 */
function appstore_oauth_provider_default_services_endpoint() {
  $endpoints = array();

  $endpoint = new stdClass;
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'appstore_oauth_provider';
  $endpoint->title = 'Appstore OAuth API';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'appstore/api';
  $endpoint->authentication = array(
    'services_oauth' => array(
      'oauth_context' => 'appstore_oauth_provider',
    ),
  );
  $endpoint->server_settings = array(
  'rest_server' => array(
    'formatters' => array(
      'bencode' => TRUE,
      'json' => TRUE,
      'php' => TRUE,
      'rss' => TRUE,
      'xml' => TRUE,
      'yaml' => TRUE,
      'jsonp' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/vnd.php.serialized' => TRUE,
      'application/x-www-form-urlencoded' => TRUE,
      'application/x-yaml' => TRUE,
      'multipart/form-data' => TRUE,
    ),
  ),
);
  $endpoint->resources = array(
    'appstore_oauth_provider' => array(
      'alias' => 'app',
      'actions' => array(
        'info' => array(
          'enabled' => 1,
          'services_oauth' => array(
            'credentials' => 'token',
            'authorization' => 'app_info',
          ),
        ),
      ),
    ),
  );
  $endpoints[$endpoint->name] = $endpoint;

  return $endpoints;
}
