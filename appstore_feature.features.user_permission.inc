<?php
/**
 * @file
 * appstore_feature.features.user_permission.inc
 */

/**
 * Implementation of hook_user_default_permissions().
 */
function appstore_feature_user_default_permissions() {
  $permissions = array();

  // Exported permission: create commerce_product entities of bundle app_release
  $permissions['create commerce_product entities of bundle app_release'] = array(
    'name' => 'create commerce_product entities of bundle app_release',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'commerce_product',
  );

  // Exported permission: edit own commerce_product entities of bundle app_release
  $permissions['edit own commerce_product entities of bundle app_release'] = array(
    'name' => 'edit own commerce_product entities of bundle app_release',
    'roles' => array(
      0 => 'authenticated user',
    ),
    'module' => 'commerce_product',
  );

  // Exported permission: view own commerce_product entities of bundle app_release
  $permissions['view own commerce_product entities of bundle app_release'] = array(
    'name' => 'view own commerce_product entities of bundle app_release',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'commerce_product',
  );

  return $permissions;
}
