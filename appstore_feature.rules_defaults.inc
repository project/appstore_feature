<?php
/**
 * @file
 * appstore_feature.rules_defaults.inc
 */

/**
 * Implementation of hook_default_rules_configuration().
 */
function appstore_feature_default_rules_configuration() {
  $items = array();
  $items['rules_order_with_full_payment'] = entity_import('rules_config', '{ "rules_order_with_full_payment" : {
      "LABEL" : "Order With Full Payment",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "commerce_payment", "commerce_order", "commerce_checkout" ],
      "ON" : [ "commerce_checkout_complete" ],
      "IF" : [ { "commerce_payment_order_balance_comparison" : { "value" : "0" } } ],
      "DO" : [ { "commerce_order_update_status" : { "order_status" : "completed" } } ]
    }
  }');
  return $items;
}
