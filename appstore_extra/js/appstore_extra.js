(function ($) {
  Drupal.behaviors.linkToClientTop = {
    attach: function (context, settings) {
      if (window.top.location.href != window.location.href) {
         $('.goto-client', context).click(function() {
          //window.top.location = window.top.location.pathname
          var top = window.top.location.href;
          top.replace(/\/$/, "");
          var new_top = top  + "/install/?fid=" + settings.fid;
          window.top.location.replace(new_top);
          return false;
        });
      }
  }};

//  Drupal.behaviors.printOnlyContent = {
//    attach: function (context, settings) {
//    if (window.top.location != window.location) {
//      $('body').html($('#header').html() + $('#content').html());
//    }
//  }};

}(jQuery));